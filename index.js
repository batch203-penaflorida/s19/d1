// if else
// switch
// try-catch-finally
/* 
   Selection Control Structures
   A selection control structor 
   Conditional statements
*/

// Programming also needs to execute decision-making tasks that depende on the input from the user.
// Conditional statements, if written in words, would read like these:
// Is the container full or not?
// Is the temperature higher than or equal to 40 degrees Celsius?
// With conditional statements, we can code predetermined actions based on user inputs.

// There are three types of conditional statements
// if else
// switch
// try catch
// console.log("Hello World");
// Control flow of our program

// [SECTIOn] if, else if, else
// Execute a statement if a specfied condition is true.
//
let numA = -1;
/* 
   Sytax:
   
   if(condition){
   
   }
*/
if (numA < 0) {
  console.log("hello");
}
// The result of the expression in the if statement must result to true else it will not run the statement inside.
// for checking the value
// console.log(numA < 0);
let city = "New York";
if (city === "New York") {
  console.log("Welcome to New York City");
}
// else if Clause
/* 
 Executes a statement if previous condition are false if the specified condition is true;
 The "else if" clause is optional
 */
let numH = 0;
if (numH < 0) {
  console.log("Hello");
} else if (numH > 0) {
  console.log("World");
}
// Executes a statement if all other condition are false.
city = "Manila";
if (city === "New York") {
  console.log("Welcome to New York City");
} else if (city === "Tokyo") {
  console.log("Welcome to Tokyo, Japan");
}

/* 
Else statement
 - Executes a statement if all other condition are false.
 - The 'else' statement is optional and can be added to capture any other result
 to change the flow of program
*/
if (numH < 0) {
  console.log("Hello");
} else if (numH > 0) {
  console.log("World");
} else {
  console.log("Again");
}

// if else if else inside a function

/* 
          Scenario: We want to determine intensity of a typhone based on its wind speed.
					Not a Typoon - Wind speed is less than 30. 
					Tropical Depression - Wind speed is less than or equal to 61.
					Tropical Storm - Wind speed is between 62 to 88.
					Severe Tropical Storm - Wind speed is between 89 to 117.
					Typoon - Wind speed is greater than or equal to 118.
*/

let message = "No message";

console.log(message);

function determineTyphoonIntensity(windSpeed) {
  if (windSpeed < 30) {
    return "Not a typhoon yet.";
  } else if (windSpeed <= 61) {
    return "Tropical Depression detected";
  } else if (windSpeed >= 62 && windSpeed <= 88) {
    return "Tropical Storm detected";
  } else if (windSpeed >= 89 && windSpeed <= 117) {
    return "Severe Tropical Storm detected";
  } else {
    return "Typhoon detected";
  }
}
message = determineTyphoonIntensity(120);
console.log(message);

// console.warn() is a good way to print warnings in our console that could help us developers
// act on certain output within our code.
if (message === "Severe Tropical Strom Detected") {
  console.warn(message);
}

/* Section Truthy and Falsy
  In javascript a truthy value is a value that is considered true when encounted in a boolean context
  falsy values/exception for truthy:
  1. false
  2. 0
  3. ""
  4. null
  5. undefined
  6. NaN
*/
isMarried = true;
// Truthy examples:
if (true) {
  console.log("Truthy");
}
if (1) {
  console.log("Truthy");
}
if ([]) {
  console.log("Truthy");
}
if (0) {
  console.log("Falsey");
}
if (false) {
  console.log("Falsey");
}
if (undefined) {
  console.log("Falsey");
}
if (isMarried) {
  console.log("Truthy");
}

//Conditional(Ternary) Operator

/* 
  - The conditional (Ternary) operator takes in three operands:
  1. Condition
  2. Expression to execute if the value is truthy.
  3. Expression to execute if value is falsy.
  - Alternative for an "if else" statement
  - Ternary operators have an "implicit return" statement meaning that
  without the "return" keyword, the result expression can be stored in a
  variable.
  - Commonly used for single statement execution where the result
  consists of only one line of code.
  - For multiple lines of code/code blocks
*/

/* 
  Syntax:
  (expression) ?  ifTrue : ifFalse;
*/

// let ternaryResult = 1 < 18 ? true : false;
// console.log("Result of Ternary Operator: " + ternaryResult);

// // Multiple statement execution using ternary operator
// // ParseInt - converts the input received into a number datatype.
// // if a input is not  a number, it will return NaN (Not a number)
// let name;

// function isOfLegalAge() {
//   name = "John";
//   return "You are of Legal age limit.";
// }
// function isUnderAge() {
//   name = "Jane";
//   return "You are under the age limit.";
// }
// // let age = parseInt(prompt("What is your age?"));

// console.log(age);

// let checkLegalAge = age >= 18 ? isOfLegalAge() : isUnderAge();
// console.log(
//   "Result of Ternary Operator in functions: " + checkLegalAge + ", " + name
// );
/* 
  We can also create nested if statements based on specific requirements:
  Nested if:
  if(expression){
    if(expression){
      //code block
    }
    else{
      //code
    }
  }
  else{
    // code block
  }
*/

// [SECTION] Switch Statement
/* 
  - The switch statement evaluates an expression and matches the expression's value to a case clause.
  - can be used as an alternative to an "if,else if, else" statement
  - The ".toLowerCase()" function/method will change the input received from the prompt to lowercase letters ensuring a match
  with switch case condition.
  - The break statement is used to terminate the current loop once a match has found.
  - Switch cases are considered "loops" meaning it will compare the "expression" with each of the case "value" until a match found.
  Syntax:
    switch(expression){
    case  value
      statement/code block;
      break;
    default: 
      statement/code block;
      break;
    }
    
*/

// let day = prompt("What day of the week is it today?").toLowerCase();
// let capitalize = day.charAt(0).toUpperCase() + day.slice(1);
// console.log(capitalize);

// switch (day) {
//   case "monday":
//     console.log("The color of the day is red.");
//     break;
//   case "tuesday":
//     console.log("The color of the day is orange.");
//     break;
//   case "wednesday":
//     console.log("The color of the day is yellow.");
//     break;
//   case "thursday":
//     console.log("The color of the day is green.");
//     break;
//   case "friday":
//     console.log("The color of the day is blue.");
//     break;
//   case "saturday":
//     console.log("The color of the day is indigo.");
//     break;
//   case "sunday":
//     console.log("The color of the day is violet.");
//     break;
//   default:
//     console.log("Please input a valid day.");
//     break;
// }

// Try-Catch-Finally Statement
/* 
  -try-catch statements are commonly used for error handling.
  -useful for debugging code of the "error" object that can be
  caught using the try catch statement.
  Syntax:
    try{
      // code block that we try to execute
    } error/err are commonly used variable names for storing errors
    catch(error/err){
      //error message.
    }
    finally{
      //continue execution of code regardless of the success and failure of the code execution.
      // optional
    }
*/

function showIntensityAlert(windSpeed) {
  try {
    alert(determineTyphoonIntensity(windSpeed));
  } catch (error) {
    // "Error.message" is used to access the information relating to an error object.
    console.warn(error.message);
  } finally {
    alert("Intensity Updates will show new alert.");
  }
}
showIntensityAlert(56);
console.log("Hello World Again");
